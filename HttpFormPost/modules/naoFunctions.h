//
//  naoFunctions.h
//  multipost
//
//  Created by naoyuki on 2013/04/20.
//  Copyright (c) 2013年 naoyuki onishi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface naoFunctions : NSObject

+ (NSString *) urlencode:(NSString *) url ;
+ (NSString *) urldecode:(NSString *) url ;

@end
