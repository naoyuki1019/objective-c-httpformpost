//
//  naoFunctions.m
//  multipost
//
//  Created by naoyuki on 2013/04/20.
//  Copyright (c) 2013年 naoyuki onishi. All rights reserved.
//

#import "naoFunctions.h"

@implementation naoFunctions


+ (NSString *) urlencode:(NSString *) url {
	return [(NSString *)CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)url, NULL, (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8) autorelease];
}


+ (NSString *) urldecode:(NSString *) url {
	return [(NSString *)CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL, (CFStringRef)url, CFSTR(""), kCFStringEncodingUTF8) autorelease];
}

@end
