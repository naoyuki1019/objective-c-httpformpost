//
//  AppDelegate.h
//  HttpFormPost
//
//  Created by naoyuki on 2014/02/24.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
