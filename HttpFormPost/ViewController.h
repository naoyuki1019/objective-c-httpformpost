//
//  ViewController.h
//  HttpFormPost
//
//  Created by naoyuki on 2014/02/24.
//
//

#import <UIKit/UIKit.h>
#import "modules/HttpFormPost.h"

@interface ViewController : UIViewController
{
	@private
	UIImageView *_image_view;
}

- (void)buttonTouch:(id)sender;

@end
